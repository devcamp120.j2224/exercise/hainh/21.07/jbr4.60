package com.devcamp.s50.jbr4_60;

import java.util.Date;

public class Visit {
    private Customer customer ;
    private Date date ;
    private double serviceExpensive;
    private double productExpensive;
    
    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }

    public Visit(Customer customer, Date date, double serviceExpensive, double productExpensive) {
        this.customer = customer;
        this.date = date;
        this.serviceExpensive = serviceExpensive;
        this.productExpensive = productExpensive;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Date getDate() {
        return date;
    }

    public double getServiceExpensive() {
        return serviceExpensive;
    }

    public double getProductExpensive() {
        return productExpensive;
    }

    public void setServiceExpensive(double serviceExpensive) {
        this.serviceExpensive = serviceExpensive;
    }

    public void setProductExpensive(double productExpensive) {
        this.productExpensive = productExpensive;
    }

    public double getTotal(){
        return productExpensive * serviceExpensive ;
    }

    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", productExpensive=" + productExpensive
                + ", serviceExpensive=" + serviceExpensive + "]";
    }
}
